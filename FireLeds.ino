#include <EEPROM.h>
#include <Arduino_JSON.h>
#include <FastLED.h>

#define LED_PIN     5
#define NUM_LEDS    31 // 17
#define LED_TYPE    WS2811
#define COLOR_ORDER GRB
CRGB leds[NUM_LEDS];
int heat[NUM_LEDS];
uint16_t energy[NUM_LEDS];

#define UPDATES_PER_SECOND 60

uint8_t cr;
uint8_t hr;
uint8_t cg;
uint8_t hg;
uint8_t cb;
uint8_t hb;
uint8_t brightness;

void setup() {
    Serial.begin(9600);
    while (!Serial);

    Serial.setTimeout(50);
  
    delay(500); // power-up safety delay (what it is for I do not know...
    FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);

    for (int i=0; i<NUM_LEDS; ++i) {
        heat[i] = 100;
    }

    cr = EEPROM.read(0);
    cg = EEPROM.read(1);
    cb = EEPROM.read(2);
    hr = EEPROM.read(3);
    hg = EEPROM.read(4);
    hb = EEPROM.read(5);
    brightness = EEPROM.read(6);

    if (cr == hr && cg == hg && cb == hb && hb == 255 && brightness == 255) {
        cr = 50;
        cg = 0;
        cb = 0;
        hr = 255;
        hg = 200;
        hb = 50;
        brightness = 128;
    }
    FastLED.setBrightness(brightness);
}

void parseJson(String json) 
{
  JSONVar readJson = JSON.parse(json);

  // JSON.typeof(jsonVar) can be used to get the type of the var
  if (JSON.typeof(readJson) == "undefined") {
    Serial.println("Parsing input failed!");
    return;
  }

  if ((bool)readJson["getValues"] == true) {
    JSONVar sendJson;
    sendJson["cr"] = cr;
    sendJson["cg"] = cg;
    sendJson["cb"] = cb;
    sendJson["hr"] = hr;
    sendJson["hg"] = hg;
    sendJson["hb"] = hb;
    sendJson["brightness"] = FastLED.getBrightness();
    Serial.println(JSON.stringify(sendJson));
    return;
  }
  
  if (JSON.typeof(readJson["cr"]) == "number" &&
        JSON.typeof(readJson["cg"]) == "number" &&
        JSON.typeof(readJson["cb"]) == "number" &&
        JSON.typeof(readJson["hr"]) == "number" &&
        JSON.typeof(readJson["hg"]) == "number" &&
        JSON.typeof(readJson["hb"]) == "number" ) 
  {
    cr = (uint8_t)(int)readJson["cr"];
    cg = (uint8_t)(int)readJson["cg"];
    cb = (uint8_t)(int)readJson["cb"];
    hr = (uint8_t)(int)readJson["hr"];
    hg = (uint8_t)(int)readJson["hg"];
    hb = (uint8_t)(int)readJson["hb"];
  }

  if (JSON.typeof(readJson["brightness"]) == "number") {
    int brightness = (int)readJson["brightness"];
    if (brightness < 0) brightness = 0;
    if (brightness > 255) brightness = 255;
    FastLED.setBrightness(brightness);    
  }
  
  if ((bool)readJson["saveValues"] == true) {
      EEPROM.write(0, cr);
      EEPROM.write(1, cg);
      EEPROM.write(2, cb);
      EEPROM.write(3, hr);
      EEPROM.write(4, hg);
      EEPROM.write(5, hb);
      EEPROM.write(6, FastLED.getBrightness());
  }

  if ((bool)readJson["getHeat"] == true) {
    for (int i=0; i<NUM_LEDS; ++i) {
        Serial.print(heat[i], DEC);
        Serial.print(" ");
    }
    Serial.println(".");
  }
}

void serialEvent() {
  static String serialData;
  
   while (Serial.available()) {
      serialData = Serial.readStringUntil('\n');
      parseJson(serialData);
      serialData = "";
    }
}

uint8_t intensityForValue(uint8_t value, uint8_t lowLimit, uint8_t highLimit)
{
    long int diff = (int)highLimit - (int)lowLimit;

    if (diff == 0) {
        return lowLimit;
    }

    int intensity = (int)lowLimit + (int)(((long int)value * diff) / 255);

    if (intensity < 0) { intensity = 0; }
    if (intensity > 255) { intensity = 255; }

    return (uint8_t)intensity;
}


CRGB FireColor(uint8_t heat)
{
    int8_t r = intensityForValue(heat, cr, hr);
    int8_t b = intensityForValue(heat, cb, hb);
    int8_t g = intensityForValue(heat, cg, hg);
    return CRGB(r, b, g);
}


static const int COOL_DOWN = 5;
static const int HEAT_UP = 7;
static const int DISIPATE_DIV = 300;

void UpdateLEDs()
{
    // cooldown all the leds a bit
    for (int i=0; i<NUM_LEDS; ++i) {
        if (heat[i] > COOL_DOWN) {
            heat[i] -= COOL_DOWN;
        }
        else {
            heat[i] = 0;
        }
    }
    
    // disipate heat to the surrounding
    for (int i=0; i<NUM_LEDS-1; ++i) {
        int16_t diff = ((int)heat[i] * 10 - (int)heat[i+1] * 10) /300;
        heat[i] -= diff;
        heat[i+1] += diff;
    }
    //heat[0] = (heat[0] * 97) / 100;
    //heat[NUM_LEDS-1] = (heat[0] * 97) / 100;
    
    int16_t total = 0;
    
    // add heat from the energy
    for (int i=0; i<NUM_LEDS; ++i) {
        if (energy[i] > HEAT_UP) {
            if (heat[i] < (255 - HEAT_UP)) {
                heat[i] += HEAT_UP;
            }
            else {
                heat[i] = 255;
            }
            energy[i] -= HEAT_UP;
        }

        if (heat[i] > 255) {
          heat[i] = 255;
          Serial.println("heat > 255");
        }
        if (heat[i] < 0) {
          heat[i] = 0;
          Serial.println("heat < 0");
        }
        total += heat[i];
        leds[i] = FireColor(heat[i]);
    }
    
    // new Energy
    if (total < (NUM_LEDS * 150)) {
        int index = random8(0, NUM_LEDS-1);
        uint16_t value = random16(100, 300);
        energy[index] += value;
        if (energy[index] > 300) energy[index] = 300;
    }
}

void TestUpdate()
{
  static int value = 0;
  for (int i=0; i<NUM_LEDS; ++i) {
    heat[i] = value;
    leds[i] = CRGB(0, 0, intensityForValue(heat[i], cg, hg));
  }
  //Serial.print(cg, DEC); 
  //Serial.print(" "); 
  //Serial.print(hg, DEC); 
  //Serial.print(" "); 
  //Serial.print(intensityForValue(value, cg, hg), DEC); 
  //Serial.print(" "); 
  //Serial.println(value, DEC);
  value++;
  if (value > 255) value = 0;
}

void loop()
{
    UpdateLEDs();

    FastLED.show();
    FastLED.delay(1000 / UPDATES_PER_SECOND);
}
